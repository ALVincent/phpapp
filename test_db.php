<?php

/**
 * Configuration for database connection
 *
 */

#can we use the old variables defined in config.php

require "config.php";

#$dbname = 'My_test_db';
#$username = 'root';
#$password = 'secret123';
$dbhost = 'academytest.chcqof7trpj1.eu-west-3.rds.amazonaws.com';
$options    = array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
              );
try {
$connect = new PDO("mysql:host=$dbhost", $username, $password, $options);
$connect->exec("use $dbname");
}
catch(PDOException $error)
{
        echo("Connection error $error");
        exit(1);
}
echo ("Connection success ");
echo ("$dbname");
$connect = null;
