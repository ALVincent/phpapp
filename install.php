<?php

/**
 * Open a connection via PDO to create a
 * new database and table with structure.
 *
 */

require "config.php";

// Create the init.sql file using our variables

$initsql=fopen("data/init.sql","w");
fwrite($initsql,"CREATE DATABASE $dbname;\n");
fwrite($initsql,"use $dbname;\n");
$table="CREATE TABLE users (
	id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	firstname VARCHAR(30) NOT NULL,
	lastname VARCHAR(30) NOT NULL,
	email VARCHAR(50) NOT NULL,
	age INT(3),
	location VARCHAR(50),
	date TIMESTAMP
);";
fwrite($initsql,$table);
fclose($initsql);

$sql = "";

try
{
	$connection = new PDO("mysql:host=$host", $username, $password, $options);
	$sql = file_get_contents("data/init.sql");
	$connection->exec($sql);

	echo "Database and table users created successfully.";
}

catch(PDOException $error2)
{
	echo $sql . "<br>" . $error2->getMessage();
}
